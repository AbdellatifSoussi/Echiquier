import java.util.ArrayList;

/**
 *
 * @author gsimon
 * Classe abstraite servant de base à la répresentation de chaque type de pièce du jeu
 */
public abstract class Piece {
    //couleurs disponibles
    public static String BLANC = "B";
    public static String NOIR = "N";
    
    //couleur de la pièce
    private String couleur;
    //coordonnées
    private int c;
    private int l;

    //une classe abstraite peut avoir un constructeur !
    //il ne sera pas utilisé pour créer une instance de la classe
    //mais il pourra être appelé dans les constructeurs des classes fille
    public Piece(String coul) {
        this.l = -1;
        this.c = -1;
        couleur = coul;
    }

    //accesseurs
    public int getC() { return c; }
    public int getL() { return l; }
    public String getCoul() { return couleur; }

    //méthode spécifique
    //permet de modifier les coordonnées de la pièce sur l'échiquier
    public void placer(int l, int c) {
        this.l = l;
        this.c = c;
    }

    public boolean isRoi() { return false; }
    
    //permet de connaitre les cases atteignables en un déplacement de la pièce
    //en tenant compte de son type de déplacement et des pièces se trouvant
    //dans les cases atteignables et, pour les rois, d'une éventuelle mise
    //en échec.
    public ArrayList<Case> getCasesPossibles(Echiquier ech) {
        return getDepPossibles(ech);
    }

    //méthodes abstraites
    
    //même chose que getCasesPossibles mais, pour les rois, ne se
    //préoccupe pas des éventuelles mises en échec
    //NB : pour les rois, méthode utilisée dans isEchec
    public abstract ArrayList<Case> getDepPossibles(Echiquier ech);
    
    @Override
    public abstract String toString();

}

