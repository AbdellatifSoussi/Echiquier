/**
 *
 * @author gsimon
 * Classe permettant de répresenter sous forme d'objet un couple de coordonnées (ligne,colonne)
 */
public class Case {
    private int l; //numéro de la ligne de la case (à partir de 0)
    private int c; //numéro de la colonne de la case (idem)

    public Case(int l, int c) {
        this.l = l;
        this.c = c;
    }

    public int getL() { return l; }
    public int getC() { return c; }

    @Override
    public boolean equals(Object o) {
        boolean res;
        
        if (o instanceof Case) {
            Case c1 = ((Case)o);
            res = ((l==c1.getL())&&(c==c1.getC()));
        }
        else {
            res = false;
        }

        return res;
    }
}
