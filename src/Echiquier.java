import java.util.ArrayList;

/**
 *
 * @author gsimon
 * Modélisation de l'échiquier
 */

public class Echiquier {
    public static final int MAXL = 8; //nombre max de lignes
    public static final int MAXC = 8; //nombre max de colonnes
    private static final String sep = "  -------------------------";

    private Piece[][] tab; //matrice représentant l'échiquier

    public Echiquier() {
        int i,j;
        tab = new Piece[MAXL][MAXC];

        for (i=0; i<MAXL; i++) {
            for (j=0; j<MAXC; j++) {
                tab[i][j] = null;
            }
        }
    }

   //renvoie vrai si la case de coordonnées (l,c) existe dans l'échiquier
    public boolean caseExiste(int l, int c) {
        boolean existe;
        
        existe = (l>=0) && (l<MAXL) && (c>=0) && (c<MAXC);
        return existe;
    }
    
    /*
     * Permet de gérer le placement initial d'une pièce sur l'échiquier
       sur une case dont on fournit les coordonnées. La méthode vérifie que 
       la case n'est pas déjà occupée par une autre pièce.
     */
    public void placer(Piece p,int l, int c) {
        Piece contenuCase;
        
        //on vérifie l'existence de la case
        if (! caseExiste(l,c)) {
            System.out.println("Placement de "+p+" sur ("+l+","+c
                        +") impossible : case choise inexistante ! ");
        }
        else {
            contenuCase = getPiece(l,c);

            if (contenuCase == null) {
                //la case cible est vide
                //il faut maintenant vérifier si la pièce à placer est un roi
                //si oui, il faut vérifier que la case choisie ne correspond pas
                //à une position d'échec
                if (! (p.isRoi())) {
                    //la pièce n'est pas un roi : on place la pièce sur la case
                    tab[l][c] = p;
                    p.placer(l,c);
                }
                else {
                    //c'est un roi. Il faut vérifier si c'est une position d'échec
                    if (isEchec(p.getCoul(),l,c)) {
                        System.out.println("Le roi "+p.getCoul()+" ne peut pas "
                                + "être placé en ("+l+","+c+") : position d'échec !");
                    }
                    else {
                        //on peut placer le roi
                        tab[l][c] = p;
                        p.placer(l,c);
                    }
                }
            } //fin then contenuCase == null
            else {
                //il y a déjà une pièce dans la case cible
                System.out.println("Placement de "+p+" sur ("+l+","+c
                        +") impossible : il y a déjà une pièce dans cette case! ");
            }
        } //fin else not caseExiste
    }

    /*
     * Permet de gérer le déplacement d'une pièce
     * Si la case cible contient une pièce adverse alors elle est prise.
     * Si la case cible contient une pièce de la même couleur alors on
     * ne fait rien.
     * Dans le cas où la case cible est erronnée on ne fait rien. On pourrait
     * aussi renvoyer une exception.
     */
    public String deplacer(int lsource, int csource, int lcible, int ccible) {
        Piece p,pieceprise;
        Case cible;
        ArrayList<Case> possibles;
        String prise=null; //chaine caractérisant la prise s'il y en a une
        
        //On affiche une information sur le déplacement 
        System.out.println("Déplacement d'une pièce de ("+lsource+","+csource
                            +") vers ("+lcible+","+ccible+")");
        
        //on vérifie que les cases source et cible existent bien
        if (! caseExiste(lsource,csource)) {
            return "Déplacement impossible : La case source n'existe pas !";
        }
        if (! caseExiste(lcible,ccible)) {
            return "Déplacement impossible : La case cible n'existe pas !";
        }
        
        //Les 2 cases existent
        cible = new Case(lcible,ccible);
        p = tab[lsource][csource];
        possibles = p.getCasesPossibles(this);

        if (possibles.contains(cible)) {
            //gérer le déplacement de la pièce
            //maj de l'échiquier
            tab[lsource][csource] = null;
            if (tab[lcible][ccible] != null) {
                //la case cible contient une pièce qui va être prise
                pieceprise = tab[lcible][ccible];
                prise = "la pièce "+pieceprise.toString()+" est prise";
            } 
            tab[lcible][ccible] = p;
            //maj des coordonnées de la pièce
            p.placer(lcible,ccible);
        }
        else {
            return "Il n'est pas possible de déplacer "
                    + "la pièce en ("+lsource+","+csource+") vers ("
                    + lcible+","+ccible+") !";
        }
        return prise;
    }

    //renvoie la pièce se trouvant éventuellement dans la case (l,c) de l'échiquier
    //sinon renvoie null
    public Piece getPiece(int l, int c) {
        return tab[l][c];
    }

    /*
     * Méthode permettant de déterminer si la case (l,c) correspond
     * à une situation de mise en échec pour un roi de la couleur
     * passée en premier paramètre.
     * A MODIFIER SI ON VEUT TENIR COMPTE DES ECHECS DES ROIS
     */
    public boolean isEchec(String coul, int l, int c) {
    	boolean isEchec = false;
    	
    	ArrayList<Piece> arPiece = new ArrayList<Piece>();
        for (int i = 0; i <= 7; i++)
        	for (int j = 0; j <= 7; j++)
        		if(getPiece(i, j) != null && getPiece(i, j).getCoul() != coul)
        			arPiece.add(getPiece(i, j));
    	
    	
        for(Piece piece : arPiece) {
			ArrayList<Case> dpPiece = piece.getDepPossibles(this);
			
			
			for (int i = 0; i < dpPiece.size(); i++) {
				if((c == dpPiece.get(i).getC()) && (l == dpPiece.get(i).getL())) {
					isEchec = true;
				}
			}
        }
        
        return isEchec;
    }
    
    /*
     * Affiche un échiquier mettant en évidence les déplacements
     * possibles de la pièce localisée aux coordonnées (l,c) 
     * s'il y en a une.
     */
    public void afficherDepPossibles(int l, int c) {
        Piece p; //la piece dont on cherche les déplacements
        ArrayList<Case> alca; //la liste des cases possibles pour le dép.
        Case ca;

        //on vérifie l'existence de la case
        if (! caseExiste(l,c)) {
            System.out.println("Afficher Déplacements possibles de la pièce en ("+l+","+c
                        +") : case choisie inexistante ! ");
            return;
        }
        
        
        //on récupère la pièce située sur la case choisie
        p = getPiece(l,c);
        if (p == null) {
            System.out.println("Afficher Déplacements possibles : "
                    + "Aucune pièce dans la case ("+l+","+c+") !");
            return;
        }

        System.out.println("Déplacements possibles de "+p+" en"+"("
                        +l+","+c+")");
        //on récupère les cases atteignables par la pièce choisie
        alca = p.getCasesPossibles(this);

        //processus d'affichage
        System.out.println("    0  1  2  3  4  5  6  7");
        for (int i=0; i<MAXL; i++) {
            System.out.println(sep);
            //affichage du numéro de la ligne
            System.out.print(i + " ");
            //affichage du reste de la ligne
            for (int j=0; j<MAXC; j++) {

                ca = new Case(i,j);
                if (alca.contains(ca)) {
                    //case possible pour le déplacement
                    //on regarde si elle contient déjà une pièce
                    if (tab[i][j] == null) {
                        //la case est vide
                        System.out.print("| #");
                    }
                    else {
                        //la case est vide, la pièce s'y trouvant va être prise
                        System.out.print("| P");
                    }
                }
                else {
                    if (tab[i][j] == null) {
                        //case non possible pour le dép et vide
                        System.out.print("|  ");
                    }
                    else {
                        //case non possible pour le dép et non vide
                        System.out.print("|"+ tab[i][j]);
                    }
                }
            } // fin for interne
            System.out.println("|");
        }
    }

    @Override
    public String toString() {
        String res = "";

        System.out.println("    0  1  2  3  4  5  6  7");
        
        for (int i=0; i<MAXL; i++) {
            res = res + sep + "\n";
            
            //affichage du numéro de la ligne
            res = res + i + " ";
            //affichage du reste de la ligne
            for (int j=0; j<MAXC; j++) {
                
                if (tab[i][j] == null) {
                    res = res + "|  ";
                } else {
                    res = res + "|" + tab[i][j];
                }
            } // fin for interne
            res = res + "|";
            res = res + "\n";
        }

        res = res + sep + "\n";
        return res;
    }
}
