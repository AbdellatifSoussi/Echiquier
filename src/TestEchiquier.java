/*
 * Tests simples avec :
 * - alternance des coups entre blanc et noir
 * - pas de mise en échec des rois
 * Dans cette version des échecs, on ne tient pas compte des mises en échec
 * éventuelles des rois que ce soit dans un déplacement ou dans le calcul
 * des déplacements possibles.
 */

/**
 *
 * @author gsimon
 */
public class TestEchiquier {

    public static void main(String[] args) {
        Echiquier echiq1;
        String placePrise; 
        
        //Création d'un échiquier vide
        echiq1 = new Echiquier(); 
        System.out.println("Echiquier vide");
        System.out.println(echiq1);

        //on positionne deux tours et deux rois
        Piece t = new Tour(Piece.BLANC);
        echiq1.placer(t, 0, 0);
        t = new Tour(Piece.NOIR);
        echiq1.placer(t, 0, 4);
        Piece r = new Roi(Piece.NOIR);
        echiq1.placer(r, 7, 3);
        r =new Roi(Piece.BLANC);
        echiq1.placer(r, 1, 3);
        System.out.println("Echiquier initial");
        System.out.println(echiq1);

        //les déplacements possibles de la tour blanche
        System.out.println();
        echiq1.afficherDepPossibles(0, 0);
        System.out.println();
        
        //Déplacement de la tour blanche en (0,4) et prenne la tour noire
        System.out.println();
        placePrise = echiq1.deplacer(0, 0, 0, 4); //on déplace la tour en haut à gauche
        if (placePrise != null) { System.out.println(placePrise); }
        System.out.println(echiq1);
        
        //les déplacements possibles du roi noir
        System.out.println();
        echiq1.afficherDepPossibles(7, 3);
        
        //Déplacement du roi noir
        System.out.println();
        placePrise = echiq1.deplacer(7, 3, 6, 3); //on déplace le roi vers le haut
        if (placePrise != null) { System.out.println(placePrise); }
        System.out.println(echiq1);
        
        //Les déplacements possibles de la tour blanche qui vient d'être déplacée
        System.out.println();
        echiq1.afficherDepPossibles(0, 4);
        System.out.println();

        //Déplacement de la tour blanche en haut à droite
        System.out.println();
        placePrise = echiq1.deplacer(0, 4, 5, 4); 
        if (placePrise != null) { System.out.println(placePrise); }
        System.out.println(echiq1);

        //Déplacements possibles du roi en (6,4)
        System.out.println();
        echiq1.afficherDepPossibles(6,3);
        
        //Essaie de déplacer le roi noir en (2,4) ! déplacement refusé !
        //Erreur : case inacessible !
        System.out.println();
        placePrise = echiq1.deplacer(6,3,2,4); 
        if (placePrise != null) { System.out.println(placePrise); }
        System.out.println(echiq1);
        
        //Enfin on déplace le roi noir sur la tour blanche 
        System.out.println();
        placePrise = echiq1.deplacer(6,3,5,4); 
        if (placePrise != null) { System.out.println(placePrise); }
        System.out.println(echiq1);
        
    }

}
