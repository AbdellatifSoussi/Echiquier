import java.util.ArrayList;

/**
 *
 * @author gsimon
 * CLASSE A COMPLETER !
 * Classe permettant de gérer les déplacements d'un roi
 * NB : Dans un premier temps, ne pas tenir compte
 * du problème de mise en échec éventuelle
 */
public class Roi extends Piece {

    public Roi(String coul) {
		super(coul);
	}

	@Override
    public boolean isRoi() { return true; }
    
    @Override
    public ArrayList<Case> getCasesPossibles(Echiquier ech) {

        /*
         * Par rapport aux autres pièces, ici il faudrait vérifier 
         * après avoir récupéré lesdéplacements possibles qu'il n'y
         * a pas de mise en échec (en utilisant isEchec de Echiquier).
         */
        
        //on calcule les cases atteignables sans tenir compte des échecs
        ArrayList<Case> adp = getDepPossibles(ech);
        
        for (int i = adp.size()-1; i >= 0; i--) {
        	if(ech.isEchec(this.getCoul(), adp.get(i).getL(), adp.get(i).getC())) {
        		adp.remove(i);
        	}
        }
        
        return adp;
    }

	@Override
	public ArrayList<Case> getDepPossibles(Echiquier echiq) {
		ArrayList<Case> CasesDisp = new ArrayList<Case>();

		if((echiq.caseExiste(this.getL()-1, this.getC()-1)) && (echiq.getPiece(this.getL()-1, this.getC()-1) == null)) {
			CasesDisp.add(new Case(this.getL()-1, this.getC()-1));
		}
		if((echiq.caseExiste(this.getL()  , this.getC()-1)) && (echiq.getPiece(this.getL()  , this.getC()-1) == null)) {
			CasesDisp.add(new Case(this.getL()  , this.getC()-1));
		}
		if((echiq.caseExiste(this.getL()+1, this.getC()-1)) && (echiq.getPiece(this.getL()+1, this.getC()-1) == null)) {
			CasesDisp.add(new Case(this.getL()+1, this.getC()-1));
		}
		
		
		if((echiq.caseExiste(this.getL()-1, this.getC())) && (echiq.getPiece(this.getL()-1, this.getC()) == null)) {
			CasesDisp.add(new Case(this.getL()-1, this.getC()));
		}
		if((echiq.caseExiste(this.getL()+1, this.getC())) && (echiq.getPiece(this.getL()+1, this.getC()) == null)) {
			CasesDisp.add(new Case(this.getL()+1, this.getC()));
		}
		
		
		if((echiq.caseExiste(this.getL()-1, this.getC()+1)) && (echiq.getPiece(this.getL()-1, this.getC()+1) == null)) {
			CasesDisp.add(new Case(this.getL()-1, this.getC()+1));
		}
		if((echiq.caseExiste(this.getL()  , this.getC()+1)) && (echiq.getPiece(this.getL()  , this.getC()+1) == null)) {
			CasesDisp.add(new Case(this.getL()  , this.getC()+1));
		}
		if((echiq.caseExiste(this.getL()+1, this.getC()+1)) && (echiq.getPiece(this.getL()+1, this.getC()+1) == null)) {
			CasesDisp.add(new Case(this.getL()+1, this.getC()+1));
		}
		
		return CasesDisp;
	}

	@Override
	public String toString() {
		return "R" + this.getCoul();
	}

}
