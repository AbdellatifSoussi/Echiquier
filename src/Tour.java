import java.util.ArrayList;

/**
 *
 * @author gsimon
 * Classe permettant de gérer les déplacement d'une tour
 * A COMPLETER !
 */
public class Tour extends Piece {

	public Tour(String coul) {
		super(coul);
	}

	@Override
	public ArrayList<Case> getDepPossibles(Echiquier echiq) {
		ArrayList<Case> CasesDisp = new ArrayList<Case>();

		boolean haut = true;
		boolean droit = true;
		boolean bas = true;
		boolean gauche = true;
		
		
		for (int i = 1; i <= 7; i++) {
			if(haut  && (echiq.caseExiste(this.getL()  , this.getC()-i))) {
				if(echiq.getPiece(this.getL(), this.getC()-i) == null)
					CasesDisp.add(new Case(this.getL(), this.getC()-i));
				else
					haut = false;
			}
			if(droit   && (echiq.caseExiste(this.getL()+i, this.getC()  ))) {
				if(echiq.getPiece(this.getL()+i, this.getC()) == null)
					CasesDisp.add(new Case(this.getL()+i, this.getC()));
				else
					droit = false;
			}	
			if(bas   && (echiq.caseExiste(this.getL()  , this.getC()+i))) {
				if(echiq.getPiece(this.getL(), this.getC()+i) == null)
					CasesDisp.add(new Case(this.getL(), this.getC()+i));
				else
					bas = false;
			}	
			if(gauche && (echiq.caseExiste(this.getL()-i, this.getC()  ))) {
				if(echiq.getPiece(this.getL()-i, this.getC()) == null)
					CasesDisp.add(new Case(this.getL()-i, this.getC()));
				else
					gauche = false;
			}
		}
		return CasesDisp;
	}

	@Override
	public String toString() {
		return "T" + this.getCoul();
	}

    
}
